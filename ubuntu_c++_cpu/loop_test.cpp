#include <stdio.h>
#include <assert.h>
#include <vector>
#include <ctime>
#include <onnxruntime_cxx_api.h>
#include <opencv2/opencv.hpp>

int main()
{
    std::string modelFilepath{"mnist_cnn_dynamic_batch_saize.onnx"};
    std::string instanceName{"mnist-classification-inference"};
    Ort::Env env(OrtLoggingLevel::ORT_LOGGING_LEVEL_WARNING,
                 instanceName.c_str());
    Ort::SessionOptions sessionOptions;
    sessionOptions.SetIntraOpNumThreads(1);
    sessionOptions.SetGraphOptimizationLevel(
        GraphOptimizationLevel::ORT_ENABLE_EXTENDED);
    Ort::Session session(env, modelFilepath.c_str(), sessionOptions);
    Ort::AllocatorWithDefaultOptions allocator;

    size_t numInputNodes = session.GetInputCount();
    size_t numOutputNodes = session.GetOutputCount();
    printf("Number of Input Nodes: %lu \n", numInputNodes);
    printf("Number of Output Nodes: %lu \n", numOutputNodes);

    const char *inputName = session.GetInputName(0, allocator);
    std::cout << "Input Name: " << inputName << std::endl;
    Ort::TypeInfo inputTypeInfo = session.GetInputTypeInfo(0);
    auto inputTensorInfo = inputTypeInfo.GetTensorTypeAndShapeInfo();
    ONNXTensorElementDataType inputType = inputTensorInfo.GetElementType();
    std::cout << "Input Type: " << inputType << std::endl;
    std::vector<int64_t> inputDims = inputTensorInfo.GetShape();
    for (int j = 0; j < inputDims.size(); j++)
    {
        printf("Input Dimensions %d : %ld \n", j, inputDims[j]);
    }
    const char *outputName = session.GetOutputName(0, allocator);
    std::cout << "Output Name: " << outputName << std::endl;
    Ort::TypeInfo outputTypeInfo = session.GetOutputTypeInfo(0);
    auto outputTensorInfo = outputTypeInfo.GetTensorTypeAndShapeInfo();
    ONNXTensorElementDataType outputType = outputTensorInfo.GetElementType();
    std::cout << "Output Type: " << outputType << std::endl;
    std::vector<int64_t> outputDims = outputTensorInfo.GetShape();
    for (int j = 0; j < outputDims.size(); j++)
    {
        printf("Out Dimensions %d : %ld \n", j, outputDims[j]);
    }

    // read image
    char name[100];
    printf("please enter image abs path\n");
    scanf("%s", name);
    cv::Mat image, imageResize, imageResizeFloat;
    image = cv::imread(name, cv::IMREAD_GRAYSCALE);
    if (!image.data)
    {
        printf("No image data");
        return -1;
    }
    // prediction from model
    // set batch to 1
    inputDims[0] = 1;
    cv::resize(image, imageResize, cv::Size(inputDims[2], inputDims[3]), 0, 0, cv::INTER_AREA);
    imageResize.convertTo(imageResizeFloat, CV_32F, 1.0 / 255);

    size_t input_tensor_size = inputDims[0] * inputDims[1] * inputDims[2] * inputDims[3];
    std::vector<float> inputTensorValues(input_tensor_size);
    inputTensorValues.assign(imageResizeFloat.begin<float>(),
                             imageResizeFloat.end<float>());

    std::vector<float> outputTensorValues(outputDims.at(1));
    std::vector<const char *> inputNames{inputName};
    std::vector<const char *> outputNames{outputName};
    // create input tensor object from data values
    int predictionResult = 0;
    float maxActivation = std::numeric_limits<float>::lowest();
    clock_t start, stop;

    start = clock(); //開始時間
    int loop = 100000;
    for (int j = 0; j < loop; j++)
    {
        auto memory_info = Ort::MemoryInfo::CreateCpu(OrtArenaAllocator, OrtMemTypeDefault);
        Ort::Value input_tensor = Ort::Value::CreateTensor<float>(memory_info, inputTensorValues.data(), input_tensor_size, inputDims.data(), 4);
        assert(input_tensor.IsTensor());
        // score model & input tensor, get back output tensor
        auto output_tensors = session.Run(Ort::RunOptions{nullptr}, inputNames.data(), &input_tensor, 1, outputNames.data(), 1);
        assert(output_tensors.size() == 1 && output_tensors.front().IsTensor());
        float *floatarr = output_tensors.front().GetTensorMutableData<float>();
        for (int i = 0; i < outputDims[1]; i++)
        {
            if (floatarr[i] > maxActivation)
            {
                maxActivation = floatarr[i];
                predictionResult = i;
            }
        }
    }
    stop = clock(); //結束時間
    printf("loop: %d , time %f sec, \n", loop, double(stop - start) / CLOCKS_PER_SEC);
    printf("image:,%s, prediction result is %d\n", name, predictionResult);
    return 0;
}