ubuntu c++ gpu inference sample code
# install
- install CUDA and cudnn first
- sudo apt-get install gcc
- sudo apt-get install g++
- sudo apt-get install libopencv-dev
- download onnxruntime-linux-x64-gpu-x.x.x.tgz from https://github.com/microsoft/onnxruntime/releases
- The onnxruntime-linux-x64-gpu-x.x.x.tgz version must conform to the corresponding cuda and cudnn, example CUDA Version 10.0, cudnn 7.6.5, onnxruntime-linux-x64-1.1.0.tgz
# build cmd
- g++ test.cpp ./onnxruntime-linux-x64-gpu-1.1.0/lib/libonnxruntime.so -I ./onnxruntime-linux-x64-gpu-1.1.0/include/ -o test.out $(pkg-config opencv --cflags --libs)
- g++ loop_test.cpp ./onnxruntime-linux-x64-gpu-1.1.0/lib/libonnxruntime.so -I ./onnxruntime-linux-x64-gpu-1.1.0/include/ -o loop_test.out $(pkg-config opencv --cflags --libs)
- copy libonnxruntime.so.1.1.0 and mnist_cnn_dynamic_batch_saize.onnx and test.out and loop_test.out in same folder
- ./test.out
- ./loop_test.out
# reference
- https://www.onnxruntime.ai/
- https://github.com/microsoft/onnxruntime/releases
- https://github.com/microsoft/onnxruntime/blob/master/csharp/test/Microsoft.ML.OnnxRuntime.EndToEndTests.Capi/CXX_Api_Sample.cpp
- https://github.com/microsoft/onnxruntime/blob/master/samples/c_cxx/model-explorer/batch-model-explorer.cpp
- https://leimao.github.io/blog/ONNX-Runtime-CPP-Inference/
- https://github.com/leimao/ONNX-Runtime-Inference/blob/main/src/inference.cpp

