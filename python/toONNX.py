import torch
import torch.onnx as torch_onnx
from trainModel import Net


def load_checkpoint(filepath):
    checkpoint = torch.load(filepath)
    model = Net()
    model.load_state_dict(checkpoint)
    for parameter in model.parameters():
        parameter.requires_grad = False
    model.eval()
    return model


model = load_checkpoint('mnist_cnn.pt')

batch_size = 1
# Input to the model
dummyInput = torch.randn(batch_size, 1, 28, 28, requires_grad=False)
torch_out = model(dummyInput)

# Export the model dynamic batch saize
torch.onnx.export(model,  # model being run
                  dummyInput,  # model input (or a tuple for multiple inputs)
                  "mnist_cnn_dynamic_batch_saize.onnx",
                  export_params=True,        # store the trained parameter weights inside the model file
                  opset_version=10,          # the ONNX version to export the model to
                  do_constant_folding=True,  # whether to execute constant folding for optimization
                  input_names=['input123'],   # the model's input names
                  output_names=['output456'],  # the model's output names
                  dynamic_axes={'input123': {0: 'batch_size'},    # variable lenght axes
                                'output456': {0: 'batch_size'}})

# Export the model with fixed batch saize
torch.onnx.export(model,  # model being run
                  dummyInput,  # model input (or a tuple for multiple inputs)
                  "mnist_cnn_fixed_batch_saize.onnx",
                  export_params=True,        # store the trained parameter weights inside the model file
                  opset_version=10,          # the ONNX version to export the model to
                  do_constant_folding=True,  # whether to execute constant folding for optimization
                  input_names=['input123'],   # the model's input names
                  output_names=['output456'],  # the model's output names
                  )
