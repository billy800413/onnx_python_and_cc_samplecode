This is for testing onnx.
# trainModel.py
- Training original pytorch mnist model.
- It will auto download trainging data before training.
- Model will be save in pt format.
# eval.py
- Load model weights to test pytorch mnist model. 
- Here you must define NN architecture(from trainModel import Net)
# toONNX.py
- Convert model to onnx version. 
- The model can be in CPU(do not need model.cuda()). 
# testONNX.py
- cpu version pip install onnxruntime, gpu version onnxruntime-gpu
- The onnxruntime-gpu version must conform to the corresponding cuda and cudnn. Example CUDA Version 10.0, cudnn 7.6.5, onnxruntime-gpu==1.1.0
# reference
- https://www.onnxruntime.ai/docs/get-started/install.html
- https://github.com/microsoft/onnxruntime/releases

