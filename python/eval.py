import torch
from trainModel import Net
from torchvision import datasets, transforms

def load_checkpoint(filepath):
    checkpoint = torch.load(filepath)
    model = Net()
    model.load_state_dict(checkpoint)
    for parameter in model.parameters():
        parameter.requires_grad = False
    model = model.cuda()
    model.eval()
    return model

model = load_checkpoint('mnist_cnn.pt')


device = torch.device("cuda")
test_kwargs = {'batch_size': 64}
cuda_kwargs = {'num_workers': 1,
                'pin_memory': True,
                'shuffle': False}
test_kwargs.update(cuda_kwargs)

transform = transforms.Compose([
    transforms.ToTensor(),
    transforms.Normalize((0.1307,), (0.3081,))
])
dataset = datasets.MNIST('../data', train=False,
                            transform=transform)
test_loader = torch.utils.data.DataLoader(dataset, **test_kwargs)

correct = 0
with torch.no_grad():
    for data, target in test_loader:
        data, target = data.to(device), target.to(device)
        output = model(data)
        pred = output.argmax(dim=1, keepdim=True)
        correct += pred.eq(target.view_as(pred)).sum().item()
print(100. * correct / len(test_loader.dataset))