import onnx
import onnxruntime
import numpy as np
import torch
from torchvision import datasets, transforms
print(onnxruntime.get_device())
onnx_model = onnx.load("mnist_cnn_dynamic_batch_saize.onnx")
onnx.checker.check_model(onnx_model)
ort_session = onnxruntime.InferenceSession(
    "mnist_cnn_dynamic_batch_saize.onnx")
outputs = [ort_session.get_outputs()[0].name]

# fake testing
x = np.random.random([1, 1, 28, 28]).astype(np.float32)
ort_inputs = {ort_session.get_inputs()[0].name: x}
ort_outs = ort_session.run(outputs, ort_inputs)

# real testing
device = torch.device("cuda")
test_kwargs = {'batch_size': 64}
cuda_kwargs = {'num_workers': 1,
               'pin_memory': True,
               'shuffle': False}
test_kwargs.update(cuda_kwargs)

dataset = datasets.MNIST('../data', train=False,
                         transform=transforms.ToTensor())
test_loader = torch.utils.data.DataLoader(dataset, **test_kwargs)

correct = 0
for data, target in test_loader:
    data, target = data.detach().cpu().numpy(), target.detach().cpu().numpy()
    ort_inputs = {ort_session.get_inputs()[0].name: data}
    ort_outs = ort_session.run(outputs, ort_inputs)
    ort_out = ort_outs[0]
    ort_out = np.argmax(ort_out, axis=1)
    correct += np.sum(np.equal(target, ort_out))
print(100. * correct / len(test_loader.dataset))
