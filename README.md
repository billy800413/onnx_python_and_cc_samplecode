test pytorch to onnx, and run model in c++, if you need install cuda and cudnn please reference
- https://medium.com/@mikethreeacer/ubuntu-18-04-%E5%AE%89%E8%A3%9D-cuda-cudnn-anaconda-pytorch-1f170b3326a4
- vim ~/.bashrc
- export PATH=$PATH:/usr/local/cuda/bin
- export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/local/cuda/lib64/
### if you want to change cuda version
- cd /usr/local/
- rm -r cuda
- ln -s cuda-10.0 cuda
- nvcc --version-


